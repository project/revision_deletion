<?php

/**
 * @file
 * Helper functions.
 */

/**
 * Get the list of revisions to delete.
 *
 * @param int $nid
 *   Specific node to retrieve.
 * @param string $header
 *   Optional table header for table sorting.
 *
 * @return array
 *   Objects of all revisions.
 */
function _revision_deletion_get_list($nid = NULL, $header = NULL) {
  $now = isset($_SERVER['REQUEST_TIME']) ? $_SERVER['REQUEST_TIME'] : time();
  $aged = $now - variable_get('revision_deletion_age');

  $subselect = db_select('node_revision', 'r')
    ->fields('r', array('nid'))
    // If there is a nid specified, skip the age check and show all revisions.
    ->condition('r.timestamp', is_null($nid) ? $aged : $now, '<');
  if ($nid) {
    $subselect->condition('r.nid', $nid);
  }
  $nodes = variable_get('revision_deletion', array());
  if ($nodes) {
    $subselect->condition('n.type', $nodes, 'IN');
  }
  $subselect->join('node', 'n', 'r.nid = n.nid AND r.vid <> n.vid');

  $query = db_select('node_revision', 'r');
  $query->fields('n', array('title', 'type', 'status'));
  $query->addField('n', 'vid', 'current');
  $query->fields('r', array('nid', 'vid', 'timestamp', 'uid', 'log'));
  $query->condition('r.timestamp', is_null($nid) ? $aged : $now, '<');
  $query->condition('n.nid', $subselect, 'IN');
  $query->join('node', 'n', 'r.nid = n.nid');
  if ($header) {
    $query = $query->extend('TableSort')->orderByHeader($header);
  }
  else {
    $query->orderBy('r.nid')->orderBy('r.timestamp', 'DESC');
  }
  $query->extend('PagerDefault')->limit(50);
  $result = $query->execute();

  $data = array();
  $previous_date = $previous_nid = $previous_vid = NULL;
  $keeper = FALSE;
  $format = variable_get('date_format_short', variable_get('date_format_short_custom', 'Y M j'));
  $format = trim(str_replace(array('H', 'i', ':'), '', $format));

  foreach ($result as $node) {
    $note = '';
    $node->select = TRUE;
    $node->class = 'revision-selected';
    $node->date = format_date($node->timestamp, 'custom', $format);

    // See if previous was an original.
    if ($node->nid != $previous_nid) {
      $previous_nid = $node->nid;
      $oldest = db_query("SELECT vid FROM {node_revision} WHERE nid = :nid ORDER BY timestamp ASC LIMIT 1", array(':nid' => $node->nid))->fetchField();
    }
    $node->oldest = $oldest;

    // Is this the current revision?
    if ($node->current == $node->vid) {
      $note = '<em>' . t('Current revision') . '</em>';
      $node->select = FALSE;
      $node->class = 'revision-current';
      // If it's not old enough yet, keep next.
      $keep_current = variable_get('revision_deletion_list_keep_current');
      if ($now - $node->timestamp < $keep_current) {
        $keeper = TRUE;
      }
    }
    else {
      // If this is the next oldest revision and the newset isn't old enough,
      // keep this one too.
      if ($keeper) {
        $note = '<em>' . t('Young current') . '</em>';
        $node->select = FALSE;
        $node->class = 'revision-young';
        $keeper = FALSE;
      }

      // Is this the original?
      $keep_original = variable_get('revision_deletion_list_keep_original');
      if ($node->vid == $oldest && $keep_original) {
        $note = '<em>' . t('Original version') . '</em>';
        $node->select = FALSE;
        $node->class = 'revision-original';
      }
      else {
        // Is it the last for the date?
        $keep_date_last = variable_get('revision_deletion_list_keep_date_last');
        if ($node->date != $previous_date && $keep_date_last) {
          $note = '<em>' . t('Last for !date', array('!date' => $node->date)) . '</em>';
          $node->select = FALSE;
          $node->class = 'revision-last';
        }
      }
    }
    $node->notes = variable_get('revision_deletion_list_show_notes') ? $note : '';

    // Save it all to be returned.
    $data[$node->vid] = $node;
    $previous_vid = $node->vid;
    $previous_date = $node->date;
  }

  return $data;
}

/**
 * Borrows heavily from the node.module api function to delete revisions.
 *
 * With some of the checks and messages removed. No check to make sure we are
 * not deleting the current node revision. That is covered in the function that
 * creates the data set.
 */
function _revision_deletion_delete_revision($nid) {
  $node = node_load(NULL, $nid);

  if (!is_object($node)) {
    watchdog('Revision Deletion', 'failed (nid @revision) not an object.', array('@revision' => $nid));
    return FALSE;
  }
  $st = node_revision_delete($node->vid);

  $array_values = array(
    '@type' => $node->type,
    '%title' => $node->title,
    '@node' => $node->nid,
    '@revision' => $node->vid,
  );

  if ($st) {
    watchdog('Revision Deletion', '%title (@type, node @node, revision @revision) deleted.', $array_values);
  }
  else {
    watchdog('Revision Deletion', '%title (@type, node @node, revision @revision) delete failed.', $array_values);
  }
}

/**
 * Delete nodes.
 *
 * @param array $nids
 *   Array of $nid's to delete.
 * @param string $destination
 *   Redirect destination.
 */
function _revision_deletion_delete_revisions(array $nids, $destination = NULL) {
  foreach ($nids as $nid) {
    _revision_deletion_delete_revision($nid);
  }
  cache_clear_all();
  drupal_set_message(format_plural(count($nids), 'Deleted 1 revision.', 'Deleted @count revisions.'));
  // drupal_goto($destination);
}

<?php

/**
 * @file
 * Node Revision Deletion admin page.
 */

require_once dirname(__FILE__) . '/revision_deletion.helpers.inc';

/**
 * Settings form.
 */
function revision_deletion_settings_form() {
  // Intervals (in seconds).
  $minute = 60;
  $hour = 60 * $minute;
  $day = 24 * $hour;
  $week = 7 * $day;

  $form['rev_del'] = array(
    '#type' => 'fieldset',
    '#title' => t('Revision Deletion settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $node_types = array();
  foreach (node_type_get_types() as $name => $type) {
    $options = variable_get("node_options_$name", array());
    $enabled = in_array('revision', $options);
    $node_types[$name] = $enabled ? '<strong>' . $type->name . '</strong>' : $type->name;
  }
  $form['rev_del']['revision_deletion'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Content types'),
    '#default_value' => variable_get('revision_deletion'),
    '#multiple' => TRUE,
    '#options' => $node_types,
    '#description' => t('Select which content types are subject to revision deletion. Types in <strong>bold</strong> have revisions enabled by default. Multiple types may be selected.'),
  );

  // Set revision frequency interval.
  $frequency = array(0 => t('Disabled'));
  $frequency_data = array(
    1 * $day,
    2 * $day,
    4 * $day,
    1 * $week,
    2 * $week,
    30 * $day,
    60 * $day,
    90 * $day,
  );
  $frequency += drupal_map_assoc($frequency_data, 'format_interval');
  $form['rev_del']['revision_deletion_frequency'] = array(
    '#type' => 'select',
    '#title' => t('Automatic deletion frequency'),
    '#default_value' => variable_get('revision_deletion_frequency'),
    '#options' => $frequency,
    '#description' => t('Frequency of the scheduled mass revision deletion.'),
  );

  // Set revision age for deletion.
  $age_data = array(
    15 * $minute,
    30 * $minute,
    1 * $hour,
    2 * $hour,
    1 * $day,
    4 * $day,
    1 * $week,
    2 * $week,
    30 * $day,
    60 * $day,
    90 * $day,
    120 * $day,
    180 * $day,
    365 * $day,
    730 * $day,
  );
  $age = drupal_map_assoc($age_data, 'format_interval');
  $form['rev_del']['revision_deletion_age'] = array(
    '#type' => 'select',
    '#title' => t('Minimum age of revision to delete'),
    '#default_value' => variable_get('revision_deletion_age'),
    '#options' => $age,
    '#description' => t('Revisions of this age and older will be deleted.'),
  );

  // Set age for current revision.
  $current_age = array(0 => t('Always'));
  $current_age_data = array(
    1 * $hour,
    2 * $hour,
    4 * $hour,
    12 * $hour,
    1 * $day,
    4 * $day,
    1 * $week,
    2 * $week,
    30 * $day,
    60 * $day,
    90 * $day,
    180 * $day,
  );
  $current_age += drupal_map_assoc($current_age_data, 'format_interval');
  $form['rev_del']['revision_deletion_list_keep_current'] = array(
    '#type' => 'select',
    '#title' => t('Minimum age of current revision'),
    '#default_value' => variable_get('revision_deletion_list_keep_current'),
    '#options' => $current_age,
    '#description' => t('If the current revision is not older than specified here, its older revisions will not be deleted, even if they are old enough. If set to "Always", older revisions will be deleted regardless of the age of the current revision.'),
  );

  // Settings for "list revisions".
  $form['list'] = array(
    '#type' => 'fieldset',
    '#title' => t('List settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#description' => t('These options are used on revisions lists.'),
  );

  // Take over core list function.
  $override = variable_get('revision_deletion_list_override');
  $form['list']['revision_deletion_list_override_before'] = array(
    '#type' => 'hidden',
    '#value' => $override,
  );
  $form['list']['revision_deletion_list_override'] = array(
    '#type' => 'checkbox',
    '#title' => t('Override revisions forms'),
    '#default_value' => $override,
    '#description' => t('If checked, the original Drupal revisions forms are replaced by those provided by this module.'),
  );

  // Show notes.
  $form['list']['revision_deletion_list_show_notes'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show revision notes'),
    '#default_value' => variable_get('revision_deletion_list_show_notes'),
    '#description' => t('Revisions forms tables will show an extra column with notes like "current" or "last for date". <em>To themers:</em> CSS classes are added to these rows to make it possible to style them differently.'),
  );

  // Keep original.
  $form['list']['revision_deletion_list_keep_original'] = array(
    '#type' => 'checkbox',
    '#title' => t('Keep original version'),
    '#default_value' => variable_get('revision_deletion_list_keep_original'),
    '#description' => t('The original version of content will be unchecked by default in deletion lists.'),
  );

  // Keep last per date.
  $form['list']['revision_deletion_list_keep_date_last'] = array(
    '#type' => 'checkbox',
    '#title' => t('Keep last revision of the day'),
    '#default_value' => variable_get('revision_deletion_list_keep_date_last'),
    '#description' => t('The last version of content of the same day will be unchecked by default in deletion lists. It is not advisable to use this option together with the <em>Keep original version</em> above.'),
  );

  $form['#submit'][] = 'revision_deletion_settings_submit';
  $form = system_settings_form($form);

  return $form;
}

/**
 * Settings form submission.
 */
function revision_deletion_settings_submit($form, &$form_state) {
  // If the override option has changed, rebuild the menu.
  if ($form_state['values']['revision_deletion_list_override_before'] != $form_state['values']['revision_deletion_list_override']) {
    menu_rebuild();
  }
  unset($form_state['values']['revision_deletion_list_override_before']);
}

/**
 * List the selected revisions and verify that the admin wants to delete them.
 */
function revision_deletion_admin_overview_confirm(&$form_state, $destination) {
  $form = array();
  $form['destination'] = array(
    '#type' => 'hidden',
    '#value' => $destination,
  );

  $form['display'] = array(
    '#type' => 'item',
    '#prefix' => '<ul>',
    '#suffix' => '</ul>',
    '#tree' => TRUE,
  );

  $revision_counter = 0;
  $edit = $form_state['values'];
  foreach (array_filter($edit['revisions']) as $nid => $value) {
    // ### ($nid);.
    $node = node_load(NULL, $nid);
    if (is_object($node) && is_numeric($node->nid)) {
      $form['display'][$nid] = array(
        '#type' => 'item',
        '#markup' => '<strong>' . check_plain($node->title) . '</strong> ' . t('(node: @node, revision: @revision)', array('@node' => $node->nid, '@revision' => $node->vid)),
        '#prefix' => '<li>',
        '#suffix' => '</li>',
      );
      $revision_counter++;
    }
  }

  if ($revision_counter == 0) {
    drupal_set_message(t('There do not appear to be any revisions to delete or your selected revisions were deleted by another administrator.'), 'warning');
    drupal_goto($destination);
  }
  else {
    return confirm_form($form, t('Are you sure you want to delete the selected revisions?'), isset($_GET['destination']) ? $_GET['destination'] : $destination, NULL, t('Delete')
    );
  }
}

/**
 * Form builder for the revisions overview administration form.
 *
 * @ingroup forms
 * @see revision_deletion_admin_overview_submit()
 */
function revision_deletion_admin_overview($form, &$form_state) {
  if (isset($form_state['storage']['confirm'])) {
    return revision_deletion_admin_overview_confirm($form_state, 'admin/config/content/revision_deletion');
  }

  $form = array();

  $form['help'] = array(
    '#type' => 'item',
    '#markup' => '<p>' . t('Using revisions is a good way to improve the integrity of your node content; however, it may result in a significant increase in your database size. This page lists the nodes that currently have revisions meeting the deletion criteria and allows you to delete them.') . '</p>' .
    '<p>' . t('Click the title to view the current content; click the revision ID to view the revision. Clicking on the <strong>Delete selected revisions</strong> button will delete all of the selected revisions, even if they are shown on other pages.') . '</p>',
  );

  $header = array(
    'vid' => array(
      'data' => t('Revision'),
      'field' => 'r.vid',
      'sort' => 'asc',
    ),
    'title' => array(
      'data' => t('Title'),
      'field' => 'n.title',
      'sort' => 'asc',
    ),
    'name' => array(
      'data' => t('User'),
    ),
    'date' => array(
      'data' => t('Created'),
      'field' => 'r.timestamp',
      'sort' => 'desc',
    ),
    'type' => array(
      'data' => t('Type'),
    ),
    'status' => t('Status'),
    'notes' => t('Notes'),
    'op' => t('Operations'),
  );

  $show_notes = variable_get('revision_deletion_list_show_notes');
  if (!$show_notes) {
    unset($header['notes']);
  }

  $revisions = _revision_deletion_get_list(NULL, $header);

  $rows = array();
  $default_value = array();
  $accounts = array();
  foreach ($revisions as $node) {
    if (!isset($accounts[$node->uid])) {
      $accounts[$node->uid] = theme('username', array('account' => user_load($node->uid)));
    }
    $default_value[$node->vid] = $node->select;
    $rows[$node->vid] = array(
      'vid' => array(
        'data' => array(
          '#type' => 'link',
          '#title' => $node->vid,
          '#href' => 'node/' . $node->nid . '/revisions/' . $node->vid . '/view',
          '#options' => array('title' => t('view revision')),
        ),
      ),
      'title' => array(
        'data' => array(
          '#type' => 'link',
          '#title' => $node->title,
          '#href' => 'node/' . $node->nid,
          '#options' => array('title' => t('view !type', array('!type' => $node->type))),
          '#prefix' => '<strong>',
          '#suffix' => '</strong><br />' . $node->log,
        ),
      ),
      'name' => $accounts[$node->uid],
      'date' => format_date($node->timestamp, 'small'),
      'type' => check_plain(node_type_get_name($node)),
      'status' => $node->status ? t('published') : t('not published'),
      'notes' => filter_xss($node->notes),
      'op' => array(
        'data' => array(
          '#type' => 'link',
          '#title' => t('list revisions'),
          '#href' => 'admin/config/content/revision_deletion/node/' . $node->nid,
        ),
      ),
    );
    if (!$show_notes) {
      unset($rows[$node->vid]['notes']);
    }
  }

  $form['revisions'] = array(
    '#type' => 'tableselect',
    '#multiple' => TRUE,
    '#header' => $header,
    '#options' => $rows,
    '#sticky' => TRUE,
    '#default_value' => $default_value,
    '#empty' => t('No content with deletable revisions found.'),
  );

  foreach ($revisions as $node) {
    if ($node->vid == $node->current) {
      $form['revisions'][$node->vid]['#disabled'] = TRUE;
    }
  }

  // Build some informational messages.
  // The values are already 'sanitized.'.
  $info_texts = array();
  $frequency = variable_get('revision_deletion_frequency');
  if ($frequency == 0) {
    $auto_msg = t('Automatic deletion is not currently scheduled.');
  }
  else {
    $auto_msg = t('Automatic deletion is scheduled to run every !interval.', array('!interval' => format_interval($frequency)));
    $last_update = variable_get('revision_deletion_cron');
    if ($last_update) {
      $msg_data = array(
        '!last_update_time' => format_date($last_update, 'large'),
        '!last_update_ago' => format_interval(time() - $last_update),
      );
      $auto_msg .= ' ' . t('It was last run !last_update_time (!last_update_ago ago).', $msg_data);
    }
    else {
      $auto_msg .= ' ' . t('It has not yet run automatically.');
    }
  }
  $info_texts[] = $auto_msg;

  $keep_current = variable_get('revision_deletion_list_keep_current');
  if ($keep_current > 0) {
    $info_texts[] = t('If the current revision was created less than !current_age ago, the next older revision will be kept.', array('!current_age' => format_interval($keep_current)));
  }
  if (variable_get('revision_deletion_list_keep_original')) {
    $info_texts[] = t('The original revision will be kept.');
  }
  if (variable_get('revision_deletion_list_keep_date_last')) {
    $info_texts[] = t('The last revision for each date will be kept.');
  }
  $age = variable_get('revision_deletion_age');
  if ($age > 0) {
    $info_texts[] = t('Revisions older than !age_interval will be deleted.', array('!age_interval' => format_interval($age)));
  }

  $form['info'] = array(
    '#type' => 'item',
    '#markup' => theme('item_list', array(
      'items' => $info_texts,
      'type' => 'ul',
    )),
  );

  $form['pager'] = array('#theme' => 'pager');

  $form['button']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Delete selected revisions'),
  );

  return $form;
}

/**
 * Process revision_deletion_admin_overview form submissions.
 */
function revision_deletion_admin_overview_submit($form, &$form_state) {
  if (!isset($form_state['storage']['confirm'])) {
    $form_state['storage']['confirm'] = $form_state['rebuild'] = TRUE;
    $form_state['storage']['revisions'] = $form_state['values']['revisions'];
  }
  else {
    _revision_deletion_delete_revisions(array_filter($form_state['storage']['revisions']), $form_state['values']['destination']);
  }
}

/**
 * Form builder for the node revisions administration form.
 *
 * @ingroup forms
 * @see revision_deletion_node_overview_submit()
 */
function revision_deletion_node_overview($form, &$form_state, $node) {
  if (isset($form_state['storage']['confirm'])) {
    return revision_deletion_admin_overview_confirm($form_state, 'admin/config/content/revision_deletion/node/' . $node->nid);
  }
  drupal_set_title(t('Revisions for "@title"', array('@title' => $node->title)));

  $form = array();

  $form['help'] = array(
    '#type' => 'item',
    '#markup' => '<p>' . t('Using revisions is a good way to improve the integrity of your node content; however, it may result in a significant increase in your database size. This page lists the nodes that currently have revisions meeting the deletion criteria and allows you to delete them.') . '</p>' .
    '<p>' . t('Click the title to view the current content; click the revision ID to view the revision. Clicking on the <strong>Delete selected revisions</strong> button will delete all of the selected revisions, even if they are shown on other pages.') . '</p>',
  );

  $header = array(
    'vid' => array(
      'data' => t('Revision'),
      'field' => 'r.vid',
      'sort' => 'asc',
    ),
    'name' => array(
      'data' => t('User'),
    ),
    'date' => array(
      'data' => t('Created'),
      'field' => 'r.timestamp',
      'sort' => 'desc',
    ),
    'notes' => t('Notes'),
    'op' => t('Operations'),
  );

  $show_notes = variable_get('revision_deletion_list_show_notes');
  if (!$show_notes) {
    unset($header['notes']);
  }

  $destination = drupal_get_destination();
  $revisions = _revision_deletion_get_list($node->nid, $header);
  $rows = array();
  $default_value = array();
  $accounts = array();

  foreach ($revisions as $revision) {
    if (!isset($accounts[$revision->uid])) {
      $accounts[$revision->uid] = theme('username', array('account' => user_load($revision->uid)));
    }
    $ops = array();
    if ($revision->vid != $revision->current) {
      $ops['revert'] = array(
        'title' => t('revert'),
        'href' => 'node/' . $revision->nid . '/revisions/' . $revision->vid . '/revert',
        'query' => $destination,
      );
      $ops['delete'] = array(
        'title' => t('delete'),
        'href' => 'node/' . $revision->nid . '/revisions/' . $revision->vid . '/delete',
        'query' => $destination,
      );
    }
    $default_value[$revision->vid] = $revision->select;
    $rows[$revision->vid] = array(
      'vid' => array(
        'data' => array(
          '#type' => 'link',
          '#title' => $revision->vid,
          '#href' => 'node/' . $revision->nid . '/revisions/' . $revision->vid . '/view',
          '#options' => array('title' => t('view revision')),
          '#suffix' => '<br />' . $revision->log,
        ),
      ),
      'name' => $accounts[$revision->uid],
      'date' => format_date($revision->timestamp, 'small'),
      'status' => $revision->status ? t('published') : t('not published'),
      'notes' => filter_xss($revision->notes),
      'op' => array(
        'data' => array(
          '#theme' => 'links__revision_deletion',
          '#links' => $ops,
          '#attributes' => array('class' => array('links', 'inline')),
        ),
      ),
    );
    if (!$show_notes) {
      unset($rows[$revision->vid]['notes']);
    }
  }

  $form['revisions'] = array(
    '#type' => 'tableselect',
    '#multiple' => TRUE,
    '#header' => $header,
    '#options' => $rows,
    '#sticky' => TRUE,
    '#default_value' => $default_value,
    '#empty' => t('No revisions found.'),
  );

  foreach ($revisions as $revision) {
    if ($revision->vid == $revision->current) {
      $form['revisions'][$revision->vid]['#disabled'] = TRUE;
    }
  }

  $form['pager'] = array('#theme' => 'pager');

  $form['button']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Delete selected revisions'),
  );

  return $form;
}

/**
 * Process revision_deletion_node_overview form submissions.
 */
function revision_deletion_node_overview_submit($form, &$form_state) {
  if (!isset($form_state['storage']['confirm'])) {
    $form_state['storage']['confirm'] = $form_state['rebuild'] = TRUE;
    $form_state['storage']['revisions'] = $form_state['values']['revisions'];
  }
  else {
    _revision_deletion_delete_revisions(array_filter($form_state['storage']['revisions']), $form_state['values']['destination']);
  }
}

/**
 * Menu callback; present an administrative revisions listing for a node.
 */
function revision_deletion_node($node = NULL) {
  // Make sure it's one of ours.
  if ($node && in_array($node->type, variable_get('revision_deletion', array()))) {
    return drupal_get_form('revision_deletion_node_overview', $node);
  }
  else {
    module_load_include('inc', 'node', 'node.pages');
    return node_revision_overview($node);
  }
}
